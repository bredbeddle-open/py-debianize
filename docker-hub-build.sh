#!/usr/bin/env bash
# To login: echo -n $(cat ~/.secrets/docker_hub_pwd) | docker login docker.io -u lordslakyr --password-stdin
# https://stackoverflow.com/questions/45517733/how-to-publish-docker-images-to-docker-hub-from-gitlab-ci

user="lordslakyr"
repo="py-debianize"
image="${user}/${repo}"

docker build -t ${image}:latest .
docker push ${image}

docker run --rm -t \
  -v $(pwd):/myvol \
  -e DOCKER_USER=${user} -e DOCKER_PASS=$(cat ~/.secrets/docker_hub_pwd) \
  -e PUSHRM_PROVIDER=dockerhub -e PUSHRM_FILE=/myvol/README.md \
  -e PUSHRM_SHORT='Generate .deb from Python project' \
  -e PUSHRM_TARGET=docker.io/${user}/${repo} -e PUSHRM_DEBUG=1 \
  chko/docker-pushrm:1
