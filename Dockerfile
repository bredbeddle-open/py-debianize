# Docker image based on python:stretch (they don't have a jessie image with 3.7)
# and FPM for building a deb from a python project.
# Credit: Based lightly on alanfranz/fpm-within-docker:debian-jessie
# Credit: Uses gist https://gist.github.com/specialunderwear/2929586
#
# To use: docker run --rm -v $(pwd):/src  docker-fpm
#   /src : Maps to the current project directory
FROM python:slim-stretch
MAINTAINER Curtis Forrester <cforrester@grayshift.com>
COPY 80-acquire-retries /etc/apt/apt.conf.d/
# RUN apt-get update && apt-get -y install apt-transport-https curl
# RUN curl https://www.franzoni.eu/keys/D401AB61.txt | apt-key add -
# RUN echo "deb https://dl.bintray.com/alanfranz/apt-current-v1-debian-jessie jessie main" > /etc/apt/sources.list.d/apt-current-v1.list

RUN apt-get update \
    && apt-get -y install apt-transport-https curl gnupg2 rubygems rubygems-integration ruby-dev ruby build-essential rsync \
    && apt-get -y dist-upgrade \
    && apt-get clean \
    && rm -f /var/lib/apt/lists/* ; rm -f /var/lib/apt/lists/partial/*

# RUN apt-get -y install apt-current
# RUN /bin/echo -e "MAXDELTA=3600\nCLEANUP_DOWNLOADED_PACKAGES=\"true\"\nCLEANUP_DOWNLOADED_LISTS=\"true\"\n" > /etc/apt-current.conf
# RUN apt-get -y dist-upgrade
RUN gem install fpm -v 1.11.0

# Because I *need* "ll"
# See: https://hhoeflin.github.io/2020/08/19/bash-in-docker/
COPY .bashrc /root

RUN mkdir -p /src/
WORKDIR /src

COPY requirements.txt /
RUN pip3 install --pre --no-cache-dir -r /requirements.txt

COPY debianize/debianize.sh /

ENTRYPOINT [ "/debianize.sh" ]
