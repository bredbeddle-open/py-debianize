# Docker Debianize Python

This Docker image is a tool to generate a ".deb" package to "debianize" a Python library. It is
executed from the Python library root directory and uses the project's ``setup.py`` to
generate the ".deb" package file. Internally, it uses `fpm` to generate the ".deb" package.

## Usage

The docker image is run from the root of the project source directory to debianize.

```bash
# Run this image from within the Python library dir
$ cd <project_dir>
# Run the container
$ docker run --rm -v $(pwd):/src bredbeddle/py-debianize -m "Curtis <curtis@example.com>"
```

To view the help output:

```bash
$ docker run --rm -v $(pwd):/src bredbeddle/py-debianize -h
```

```
Usage: debianize.sh -m "nobody <nobody@example.com>" -i django -i build_this_too
   -m The maintainer string ("nobody <nobody@example.com>")
   -i Using this flag makes following dependencies explicit. It will only
      build dependencies listed in install_requires that match the regex
      specified after -i. Use -i multiple times to specify multiple packages
   -I Use this flag to ignore a dependencies from being built. Packages that
      match the regex specified after -I. Use -I multiple times add more ignore
      regexes.
   -f full path to fpm binary to use.
   -p full path to pip binary to use.


All flags are optional
Anything after an unknown flag has been encountered, will be passed to fpm as arguments.
-i and -I are mutually exclusive.
```

To run the Docker container interactively (you know - to fiddle manually with things):

```bash
$ docker run --rm -v $(pwd):/src -it --entrypoint="/bin/bash" bredbeddle/py-debianize
```

To view the contents of the built-deb file:

```bash
$ dpkg-deb -c foo.deb
```

## Project Setup

If the Python project has dependencies that should be included, they need to be listed in the
`setup.py` within the `install_requires` variable. This is optional if it is known that the
dependencies will also be independently installed and present. (For public libraries it is
good form to list all dependencies.)

```python
setuptools.setup(
    version='0.1.2.0',
    install_requires=[
        'six',
    ],
)
```

## Credits

> 
> * Based lightly on alanfranz/fpm-within-docker:debian-jessie 
> * Uses gist https://gist.github.com/specialunderwear/2929586